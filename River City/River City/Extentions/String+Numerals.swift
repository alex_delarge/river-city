//
//  String+Numerals.swift
//  River City
//
//  Created by Алексей Петров on 06/01/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
// 

import Foundation

extension String {
    static func getWord(number: Int, words: Array<String>) -> String! {
        var word: String = ""
        var num: Int = number
        num = num % 100
        if num >= 11 && num <= 19 {
            word = words[2]
        } else {
            let increment: Int = num % 10
            switch increment {
            case 1:
                word = words[0]
                break
            case 2:
                fallthrough
            case 3:
                 fallthrough
            case 4:
                word = words[1]
                break
            default:
                word = words[2]
                break
            }
        }
        return word
    }
}
