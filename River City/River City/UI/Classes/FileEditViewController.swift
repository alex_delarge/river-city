//
//  FileEditViewController.swift
//  River City
//
//  Created by Алексей Петров on 06/01/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

class FileEditViewController: UIViewController, UITextViewDelegate {
    private var fileRedactor: FileRedactor = FileRedactor(file: FileStruct())
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    func configFileRedactor(atPath: String) {
        fileRedactor = FileRedactor(path: atPath)
    }
    
    override func viewDidLoad() {
        sizeLabel.text = fileRedactor.getFileSize()
        textView.text = fileRedactor.readFile()
        self.title = fileRedactor.getFileName()
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        let text: String = textView.text
        sizeLabel.text = String(format: "%i Б", text.utf8.count)
    }
    
    @IBAction func save(_ sender: Any) {
        fileRedactor.writeFile(data: textView.text)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            textView.contentInset = UIEdgeInsets.zero
        } else {
            textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        textView.scrollIndicatorInsets = textView.contentInset
        
        let selectedRange = textView.selectedRange
        textView.scrollRangeToVisible(selectedRange)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
}
