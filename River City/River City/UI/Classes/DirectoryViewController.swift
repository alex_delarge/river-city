//
//  File.swift
//  Rive/////////////r City
//
//  Created by Алексей Петров on 24/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit





class DirectoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let Identifier = "infoCell"
    var dataManager: DataManager = DataManager()
    var textField: UITextField = UITextField()
    
    @IBOutlet weak var newItemsToolbar: UIToolbar!
    @IBOutlet weak var deleteToolbar: UIToolbar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    
    
    func configDataManager(withPath: String) {
        dataManager = DataManager.init(path: withPath)
    }
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        if self.isMovingFromParent {
            let parent = dataManager.getParentFolder()
             debugPrint("DirectoryViewController", parent)
            Creator.defaultCreator.changeCurrentFolder(atPath: parent)
           
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        dataManager.updateCurrentState()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        dataManager.updateCurrentState()
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        deleteToolbar.isHidden = true
        tableView.tableFooterView = UIView()
        self.title = dataManager.getNameOfCurrentDirectory()
        
    }
    
    private func configurationTextField(textField: UITextField!)
    {
        if textField != nil {
            self.textField = textField!
            self.textField.placeholder = "Имя папки"
        }
    }
    
    // mark - UITableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataManager.getContensNumber()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: self.Identifier, for: indexPath)
        let currentItem: DirectoryItem = dataManager.getContents(atIndexPath: indexPath)
        if currentItem.isFile {
            cell.accessoryType = .none
        }
        if currentItem.isFolder {
            cell.accessoryType = .disclosureIndicator
        }
        cell.textLabel?.text = currentItem.name
        cell.detailTextLabel?.text = currentItem.info
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let cell = tableView.cellForRow(at: indexPath)
            let currentItem: DirectoryItem = dataManager.getItem(atName: cell?.textLabel?.text ?? "")
            if currentItem.isFolder {
                if Creator.defaultCreator.deleteFolder(path: self.dataManager.getFolderPath(indexPath: indexPath)) {
                    self.dataManager.updateCurrentState()
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                }
            }
            if currentItem.isFile {
                if Creator.defaultCreator.deleteFile(atPath: dataManager.getFilePath(atName: currentItem.name)) {
                    self.dataManager.updateCurrentState()
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                }
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !tableView.isEditing {
            tableView.deselectRow(at: indexPath, animated: true)
            let cell = tableView.cellForRow(at: indexPath)
            
            let currentContex: DirectoryItem = dataManager.getItem(atName: cell?.textLabel?.text ?? "")
            if currentContex.isFolder {
                Creator.defaultCreator.changeCurrentFolder(atPath: dataManager.getFolderPath(indexPath: indexPath))
            
                let directoryViewController: DirectoryViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DirectoryViewController") as! DirectoryViewController
                directoryViewController.configDataManager(withPath: dataManager.getFolderPath(indexPath: indexPath))
                self.navigationController?.pushViewController(directoryViewController, animated: true)
            
            } else {
                let fileEditViewController: FileEditViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FileEditViewController") as! FileEditViewController
                fileEditViewController.configFileRedactor(atPath: dataManager.getFilePath(atName: currentContex.name))
                self.navigationController?.pushViewController(fileEditViewController, animated: true)
            }
        }
    }
    
    // mark - actions
    
    @IBAction func createNewFolder(_ sender: Any) {
        let alert = UIAlertController(title: "Новая папка", message: "Введите имя", preferredStyle: UIAlertController.Style.alert)
        
        alert.addTextField(configurationHandler: configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            print(self.textField.text ?? "новая папка")
            let name: String = self.textField.text ?? "Dick"
            if Creator.defaultCreator.createFolder(name: name) {
                self.dataManager.updateCurrentState()
                self.tableView.reloadData()
            }
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @IBAction func createNewFile(_ sender: Any) {
        let alert = UIAlertController(title: "Новый файл", message: "Введите имя", preferredStyle: UIAlertController.Style.alert)
        
        alert.addTextField(configurationHandler: configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                    let name: String = self.textField.text ?? ""
                    if name.contains(".") {
                        if Creator.defaultCreator.createFile(name: name, contents: nil) {
                            self.dataManager.updateCurrentState()
                            self.tableView.reloadData()
                        }
                    } else {
                         let errorAlert = UIAlertController(title: "Вы не указали тип файла!", message: "например .txt ", preferredStyle: UIAlertController.Style.alert)
                        errorAlert.addAction(UIKit.UIAlertAction(title: "Ok", style: .default, handler: nil))
                       self.present(errorAlert, animated: true, completion: nil)
                    }
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func deleteSelectedItems(_ sender: UIBarButtonItem) {
        if let selectedRows = tableView.indexPathsForSelectedRows {
            var paths: Array<String> = Array()
            for indexPath in selectedRows {
                let cell = tableView.cellForRow(at: indexPath)
                let currentContex: DirectoryItem = dataManager.getItem(atName: cell?.textLabel?.text ?? "")
                if currentContex.isFolder {
                     paths.append(dataManager.getFolderPath(indexPath: indexPath))
                } else {
                    paths.append(dataManager.getFilePath(atName: currentContex.name))
                }
                
            }
            for path in paths  {
                if path.contains(".") {
                    if Creator.defaultCreator.deleteFile(atPath: path) {
                         dataManager.updateCurrentState()
                    }
                } else if !path.contains(".") {
                    if Creator.defaultCreator.deleteFolder(path: path) {
                        dataManager.updateCurrentState()
                    }
                }
                
                
            
            }
            tableView.beginUpdates()
            tableView.deleteRows(at: selectedRows, with: .automatic)
            tableView.endUpdates()
        }
        self.tableView.isEditing = false
//        editButtonItem.title = "Edit"
        editButton.title = "Edit"
        deleteToolbar.isHidden = true
        newItemsToolbar.isHidden = false
    }
    
    @IBAction func enterEditMode(_ sender: UIBarButtonItem) {
        if tableView.visibleCells.count != 0 {
            if(self.tableView.isEditing == true)
            {
                self.tableView.isEditing = false
                sender.title = "Edit"
                deleteToolbar.isHidden = true
                newItemsToolbar.isHidden = false
            }
            else
            {
                self.tableView.isEditing = true
                sender.title = "Cancel"
                deleteToolbar.isHidden = false
                newItemsToolbar.isHidden = true
            }
        } else  {
            if !tableView.isEditing {
            let errorAlert = UIAlertController(title: "нет файлов и папок", message: nil, preferredStyle: UIAlertController.Style.alert)
            errorAlert.addAction(UIKit.UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(errorAlert, animated: true, completion: nil)
            }
        }
    }
    
    
}
