//
//  FileRedactor.swift
//  River City
//
//  Created by Алексей Петров on 05/01/2019.
//  Copyright © 2019 Алексей Петров. All rights reserved.
//

import Foundation


class FileRedactor {
    
    private let coreDataManager: CoreDataManager = CoreDataManager()
    private var currentFile: FileStruct = FileStruct()
    
    init(path: String) {
         currentFile = coreDataManager.readRecord(atPath: path, type: coreDataManager.kFileEntity) as! FileStruct
    }
    
    init(file: FileStruct) {
        currentFile = file
    }
    
    func getFileSize() -> String! {
        return currentFile.size
    }
    
    func getFileName() -> String! {
        return currentFile.name
    }
    
    private func updateFileInfo() {
        let arrayOfFolders: Array<FolderStruct> = coreDataManager.readRecords(type: coreDataManager.kFolderEntity) as! Array<FolderStruct>
        for folder in arrayOfFolders {
            let folderInfo: FolderInfo = FileSystemManager.defaultManager.updateinfo(atPath: folder.path)
            var newFolderInfo: FolderStruct = folder
            newFolderInfo.size = folderInfo.size
            coreDataManager.update(folderInfo: newFolderInfo)
        }
        coreDataManager.update(fileInfo: currentFile)
    }
    
    func readFile() -> String! {
        return FileSystemManager.defaultManager.readFromFile(path: currentFile.path)
    }
    
    func writeFile(data: String) {
        if FileSystemManager.defaultManager.writeToFile(data: data, path: currentFile.path) {
            currentFile.size = FileSystemManager.defaultManager.getFileSize(atPath: currentFile.path)
            updateFileInfo()
        }
    }
}
