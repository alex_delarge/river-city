//
//  FileManager.swift
//  River City
//
//  Created by Алексей Петров on 14/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation

struct FolderInfo {
    var folders: Int! = 0
    var contents: Array<Any> = Array()
    var size: String! = ""
    var files: Int = 0
    
}


class FileSystemManager {
    
    static let defaultManager = FileSystemManager(rootDirectory: "root")
    private let nativeManager: FileManager
    var currentFolder: String = ""
    private let basePath: String
    
     init(rootDirectory: String) {
        nativeManager = FileManager.default
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        nativeManager.changeCurrentDirectoryPath(path.first!)
        basePath = nativeManager.currentDirectoryPath
        if !chek(name: rootDirectory, atPath: nativeManager.currentDirectoryPath) {
                let rootPath = URL.init(fileURLWithPath: createPath(withName: rootDirectory))
                do {
                    try nativeManager.createDirectory(at: rootPath, withIntermediateDirectories: false, attributes: nil)
                    nativeManager.changeCurrentDirectoryPath(createPath(withName: rootDirectory))
                    currentFolder = nativeManager.currentDirectoryPath
                } catch let error {
                        debugPrint("Error creating directory: \(error.localizedDescription)")
                }
        } else {
            change(currentPath: rootDirectory)
        }
    }
    
    private func createPath(withName: String) -> String! {
        return nativeManager.currentDirectoryPath + "/" + withName
    }
    
    private func chek(name: String, atPath: String) -> Bool {
        let filesInDirectory = try! self.nativeManager.contentsOfDirectory(atPath: atPath)
        let files = filesInDirectory
        
        if files.count > 0 {
            for item: String in files {
                if item == name {
                    return true
                }
            }
        }
        return false
    }
    
    func createFolder(name: String) -> Bool! {
        if  !chek(name: name, atPath: nativeManager.currentDirectoryPath) {
            let folderPath: String = nativeManager.currentDirectoryPath + "/" + name
            do {
                try nativeManager.createDirectory(at: URL.init(fileURLWithPath: folderPath), withIntermediateDirectories: false, attributes: nil)
                return true
            } catch let error {
                debugPrint("Error creating directory: \(error.localizedDescription)")
            }
        } else {
            return false
        }
       return false
    }
    
    func change(currentPath: String){
        nativeManager.changeCurrentDirectoryPath(basePath + "/" + currentPath)
    }

    
    func removeFolder(name: String!, atParentPath: String!) -> Bool {
        let path: String = basePath + "/" + atParentPath
        if chek(name: name, atPath: path) {
            do {
                try nativeManager.removeItem(at: URL.init(fileURLWithPath: basePath + "/" + atParentPath  + "/" + name))
                return true
            } catch let error {
                debugPrint(error.localizedDescription)
                return false
            }
        }
        return false
    }
    
    func removeFile(atPath: String) -> Bool {
        let path: String = basePath + "/" + atPath
        do {
            try nativeManager.removeItem(atPath: path)
            return true
        } catch let error {
            debugPrint(error.localizedDescription)
            return false
        }
    }
    
    func updateinfo(atPath: String) -> FolderInfo! {
        var result: FolderInfo = FolderInfo()
        let path: String = basePath  + "/" + atPath
        do {
            let contents = try FileManager.default.contentsOfDirectory(atPath: path)
            var folderSize: Int64 = 0
            for content in contents {
                do {
                    let fullContentPath = path + "/" + content
                    let fileAttributes = try FileManager.default.attributesOfItem(atPath: fullContentPath)
                    folderSize += fileAttributes[FileAttributeKey.size] as? Int64 ?? 0
                } catch _ {
                    continue
                }
            }
            let fileSizeStr = ByteCountFormatter.string(fromByteCount: folderSize, countStyle: ByteCountFormatter.CountStyle.file)
            result.size = fileSizeStr
//               nativeManager.enumerator(atPath: path)
           
            result.contents = try! self.nativeManager.contentsOfDirectory(atPath: path)
//            result.folders = result.contents.count
            return result

        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
    
    func createFile(name: String, data: Data!) -> Bool! {
        let folderPath: String = nativeManager.currentDirectoryPath + "/" + name
        if nativeManager.createFile(atPath: folderPath, contents: data, attributes: nil) {
            debugPrint(folderPath)
            return true
        }
        return false
    }
    
    func getFileSize(atPath: String) -> String! {
       let path: String = basePath  + "/" + atPath
       var fileSize : Int64 = 0
        
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: path)
            fileSize = attr[FileAttributeKey.size] as! Int64

        } catch {
            debugPrint("Error: \(error)")
        }
        return ByteCountFormatter.string(fromByteCount: fileSize, countStyle: ByteCountFormatter.CountStyle.file)
    }
    
    func readFromFile(path: String) -> String! {
        let filePath: String = basePath + "/" + path
        var readingString: String = ""
        do {
            let fileURL: URL = URL(fileURLWithPath: filePath)
            readingString = try String(contentsOf: fileURL, encoding: String.Encoding.utf8)
            return readingString
        } catch let error {
            debugPrint("FileSystemMAnager readFromFile", error.localizedDescription)
            return nil
        }
    }
    
    func writeToFile(data: String!, path: String) -> Bool {
        let filePath: String = basePath + "/" + path
       
        do {
             let fileURL: URL = URL(fileURLWithPath: filePath)
            try data.write(to: fileURL, atomically: false, encoding: String.Encoding.utf8)
            return true
        } catch let error {
            debugPrint("FileSystemMAnager writeToFile", error.localizedDescription)
            return false
        }
    }
}
