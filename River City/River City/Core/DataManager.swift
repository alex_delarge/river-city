//
//  DataManager.swift
//  River City
//
//  Created by Алексей Петров on 25/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import UIKit

struct DirectoryItem {
    var info: String! = ""
    var name: String = ""
    var isFile: Bool = false
    var isFolder = false
    
}

class DataManager {
    private let coreDataManager: CoreDataManager = CoreDataManager()
    private let rootFolder: String
    private var currentFolder: FolderStruct = FolderStruct()
    private var currentContext: Array<DirectoryItem> = Array()
    
    
    init(path: String) {
        rootFolder = Creator.defaultCreator.getPathOfCurrentFolder()
        if (coreDataManager.rootExist()) {
            currentFolder = coreDataManager.readRecord(atPath: path)
        }
    }
    
    init() {
        rootFolder = Creator.defaultCreator.getPathOfCurrentFolder()
        if (coreDataManager.rootExist()) {
            currentFolder = coreDataManager.readRecord(atPath: rootFolder)
        }
    }
    
    func updateCurrentState() {
        currentFolder = coreDataManager.readRecord(atPath: currentFolder.path)
    }
    
    func getContensNumber() -> Int {
        if currentFolder.contents == nil {
            return 0
        }
        return currentFolder.contents.count
    }
    
    func getNameOfCurrentDirectory() -> String {
        return currentFolder.name
    }
    
    func getParentFolder() -> String {
        return currentFolder.parent
    }
    
    func getItem(atName: String) -> DirectoryItem! {
        
        for data in currentContext as [DirectoryItem] {
            if data.name == atName {
                return data
            }
        }
        return nil
    }
    
    func getContents(atIndexPath: IndexPath) -> DirectoryItem {
        let result: DirectoryItem = getNext(item: currentFolder.contents[atIndexPath.row] as! String)
        currentContext.append(result)
        return result
    }
    
    func getNext(item: String) -> DirectoryItem! {
       
        for data in currentFolder.filesList as [String] {
            if item == data {
                let path: String = currentFolder.path + "/" + item
                let file: FileStruct = coreDataManager.readRecord(atPath: path, type: coreDataManager.kFileEntity) as! FileStruct
                 var returnInfo: DirectoryItem = DirectoryItem()
                returnInfo.isFile = true
                returnInfo.info = file.size
                returnInfo.name = file.name
                return returnInfo
            }
        }
        for data in currentFolder.foldersList as [String] {
            if data == item {
                let path: String = currentFolder.path + "/" + item
                let folder: FolderStruct = coreDataManager.readRecord(atPath: path, type: coreDataManager.kFolderEntity) as! FolderStruct
                var returnInfo: DirectoryItem = DirectoryItem()
                returnInfo.isFolder = true
                returnInfo.name = folder.name
                if folder.filesList.count > 0 {
                    let foldersWord = String.getWord(number: folder.folders, words: ["папка", "папки", "папок", ""])!
                    let filesWord = String.getWord(number: folder.filesList.count, words: ["файл", "файла", "файлов", ""])!
                    returnInfo.info = String(format: "%@, %i %@, %i %@", folder.size, folder.folders, foldersWord, folder.filesList.count, filesWord)
                } else {
                    let foldersWord = String.getWord(number: folder.folders, words: ["папка", "папки", "папок", ""])!
                    returnInfo.info = String(format: "%@, %i %@", folder.size, folder.folders, foldersWord)
                }
                return returnInfo
            }
        }
        return nil
    }
    
    func getFolderPath(indexPath: IndexPath) -> String! {
        var path: String = currentFolder.path + "/" //+ currentFolder.contents[atIndexPath.row] as! String
        var folder: FolderStruct = FolderStruct()
//        if indexPath.row < currentFolder.contents.count {
            path.append(currentFolder.contents[indexPath.row] as! String)
            folder = coreDataManager.readRecord(atPath: path)
//        }
        return folder.path
    }
    
    func getFilePath(atName: String) -> String! {
        let path: String = currentFolder.path + "/" + atName
        let file: FileStruct = coreDataManager.readRecord(atPath: path, type: coreDataManager.kFileEntity) as! FileStruct
        return file.path
    }
    
    func getName(indexPath: IndexPath) -> String! {
        return currentFolder.contents[indexPath.row] as? String
    }
}
