//
//  CoreDataManager.swift
//  River City
//
//  Created by Алексей Петров on 20/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation
import CoreData
import UIKit


class CoreDataManager {
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let kFileEntity = "File"
    let kFolderEntity = "Folder"
    init() {
        
    }
    
    // updating
    
    func update(folderInfo: FolderStruct) {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: kFolderEntity)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                if data.value(forKey: "path") as? String == folderInfo.path {
                data.setValue(folderInfo.size, forKey: "size")
                data.setValue(folderInfo.folders, forKey: "folders")
                data.setValue(folderInfo.contents, forKey: "contents")
                data.setValue(folderInfo.filesCount, forKey: "files")
                data.setValue(folderInfo.filesList, forKey: "files_list")
                data.setValue(folderInfo.foldersList, forKey: "folders_list")
                }
            }
            do {
                try context.save()
            } catch let error {
               debugPrint("CoreDataManager update folderInfo", error.localizedDescription)
            }
        } catch let error {
             debugPrint("CoreDataManager update folderInfo", error.localizedDescription)
        }
    }
    
    func update(fileInfo: FileStruct)  {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: kFileEntity)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                if data.value(forKey: "path") as? String == fileInfo.path {
                    data.setValue(fileInfo.size, forKey: "size")
                }
            }
            do {
                try context.save()
            } catch let error {
                debugPrint("CoreDataManager update fileInfo", error.localizedDescription)
            }
            
        } catch let error {
            debugPrint("CoreDataManager update fileInfo", error.localizedDescription)
        }
    }
    
    // reading
    
    func readRecord(atPath: String, type: String) -> Any! {
        switch type {
        case kFolderEntity:
            var result: FolderStruct = FolderStruct()
            let arrrayOfRecords = readRecords(type: kFolderEntity)
            for record in arrrayOfRecords as! [FolderStruct] {
                if record.path == atPath {
                    result = record
                }
            }
            return result
        case kFileEntity:
            var result: FileStruct = FileStruct()
            let arrrayOfRecords = readRecords(type: kFileEntity)
            for record in arrrayOfRecords as! [FileStruct] {
                if record.path == atPath {
                    result = record
                }
            }
            return result
        default:
            return nil
        }
    }
    
    
    func readRecord(atPath: String) -> FolderStruct! {
        var result: FolderStruct = FolderStruct()
        let arrrayOfRecords = readRecords(type: kFolderEntity)
        for record in arrrayOfRecords as! [FolderStruct] {
            if record.path == atPath {
                result = record
            }
        }
        return result
    }
    
    func isExist(atPath: String!, type: String!) -> Bool {
        let arrrayOfRecords = readRecords(type: type)
        switch type {
        case kFolderEntity:
            for record in arrrayOfRecords as! [FolderStruct] {
                if record.path == atPath {
                    return true
                }
            }
        case kFileEntity:
            for record in arrrayOfRecords as! [FileStruct] {
                if record.path == atPath {
                    return true
                }
            }
        default:
            return false
        }
        
        return false
    }
    
    func rootExist() -> Bool {
        let arrrayOfRecords = readRecords(type: kFolderEntity)
        for record in arrrayOfRecords as! [FolderStruct] {
            if record.name == "root" {
                return true
            }
        }
        return false
    }
    
    func readRecords(type: String!) -> Array<Any>! {
        let context = appDelegate.persistentContainer.viewContext
            switch type {
            case kFolderEntity:
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: type)
                request.returnsObjectsAsFaults = false
                 let sort = NSSortDescriptor(key: "date_of_creation", ascending: true)
                request.sortDescriptors = [sort]
                var returnedArray: Array<FolderStruct> = Array()
                do {
                    let result = try context.fetch(request)
                    for data in result  as! [NSManagedObject]  {
                        var folder: FolderStruct = FolderStruct()
                        folder.contents = data.value(forKey: "contents") as? Array<Any>
                        folder.type = data.value(forKey: "type") as? String
                        folder.filesList = data.value(forKey: "files_list") as? Array<String>
                        folder.foldersList = data.value(forKey: "folders_list") as? Array<String>
                        folder.folders = data.value(forKey: "folders") as? Int
                        folder.name = data.value(forKey: "name") as? String
                        folder.path = data.value(forKey: "path") as? String
                        folder.parent = data.value(forKey: "parent") as? String
                        folder.size = data.value(forKey: "size") as? String
                        folder.date = data.value(forKey: "date_of_creation") as? Date
                        returnedArray.append(folder)
                    }
                } catch let error {
                    debugPrint("CoreDataManager, folder", error.localizedDescription)
                    return nil
                }
                return returnedArray
                
            case kFileEntity:
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: type)
                request.returnsObjectsAsFaults = false
                let sort = NSSortDescriptor(key: "creation_date", ascending: true)
                request.sortDescriptors = [sort]
                var returnedArray: Array<FileStruct> = Array()
                do {
                   
                    let result = try context.fetch(request)
                    for data in result  as! [NSManagedObject]  {
                        var file: FileStruct = FileStruct()
                        file.size = data.value(forKey: "size") as? String
                        file.type = data.value(forKey: "type") as? String
                        file.name = data.value(forKey: "name") as? String
                        file.path = data.value(forKey: "path") as? String
                        file.date = data.value(forKey: "creation_date") as? Date
                        returnedArray.append(file)
                    }
                } catch let error {
                    debugPrint("CoreDataManager, file", error.localizedDescription)
                    return nil
                }
                return returnedArray
            default:
                return nil
            }
    }
    
    func delete(record: FolderStruct!) -> Bool {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: kFolderEntity)
        request.returnsObjectsAsFaults = false
        do {
            let coreResult = try context.fetch(request)
            for data in coreResult as! [NSManagedObject] {
                if data.value(forKey: "path") as? String == record.path {
                    context.delete(data)
                    return true
                }
            }
        } catch {
            return false
        }
        
        do {
            try context.save()
        } catch {
            return false
        }
        return false
    }
    
    func delete(record: FileStruct!) -> Bool {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: kFileEntity)
        request.returnsObjectsAsFaults = false
        do {
            let coreResult = try context.fetch(request)
            for data in coreResult as! [NSManagedObject] {
                if data.value(forKey: "path") as? String == record.path {
                    context.delete(data)
                    return true
                }
            }
        } catch {
            return false
        }
        
        do {
            try context.save()
        } catch {
            return false
        }
        return false
    }
    
    // creation
    
    func create(record: FolderStruct) {
        if !isExist(atPath: record.path, type: kFolderEntity) && (record.path != nil) {
            let context = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: kFolderEntity, in: context)
            let newRecord = NSManagedObject(entity: entity!, insertInto: context)
            newRecord.setValue(record.name, forKey: "name")
            newRecord.setValue(record.type, forKey: "type")
            newRecord.setValue(record.parent, forKey: "parent")
            newRecord.setValue(record.path, forKey: "path")
            newRecord.setValue(record.date, forKey: "date_of_creation")
            do {
                try context.save()
            } catch {
                print("Failed saving")
            }
        } else {
            print("exist, you're motherfucker!!!")
        }
    }
    
    
    
    func create(record: FileStruct) {
        if !isExist(atPath: record.path, type: kFileEntity) {
            let context = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: kFileEntity, in: context)
            let newRecord = NSManagedObject(entity: entity!, insertInto: context)
            newRecord.setValue(record.name, forKey: "name")
            newRecord.setValue(record.size, forKey: "size")
            newRecord.setValue(record.path, forKey: "path")
            newRecord.setValue(record.type, forKey: "type")
            newRecord.setValue(record.date, forKey: "creation_date")
            do {
                try context.save()
            } catch let error{
                debugPrint("CoreDataManager, Creation File ", error.localizedDescription)
            }
        } else {
            debugPrint("exist", record.name, record.date)
        }
    }
}
