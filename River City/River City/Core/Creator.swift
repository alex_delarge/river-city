//
//  Creator.swift
//  River City
//
//  Created by Алексей Петров on 19/12/2018.
//  Copyright © 2018 Алексей Петров. All rights reserved.
//

import Foundation


struct FolderStruct {
    var type: String! = ""
    var name: String! = ""
    var date: Date! = Date()
    var path: String! = ""
    var size: String! = ""
    var parent: String! = ""
    var folders: Int! = 0
    var filesCount: Int! = 0
    var filesList: Array<String>! = Array()
    var foldersList: Array<String>! = Array()
    var contents: Array<Any>! = Array()
}

struct FileStruct {
    var type: String! = ""
    var name: String! = ""
    var size: String! = ""
    var path: String! = ""
    var date: Date! = Date()
}

class Creator {
    
    static let defaultCreator = Creator()
    private let coreDataManager: CoreDataManager
    private var currentFolder: String
    
    init() {
        currentFolder = "root"

        coreDataManager = CoreDataManager()
        
        var rootFolder = FolderStruct()
        if !coreDataManager.rootExist() {
            rootFolder.path = currentFolder

            rootFolder.name = "root"
            rootFolder.parent = ""
            rootFolder.type = coreDataManager.kFolderEntity
            coreDataManager.create(record: rootFolder)
        } else {

            rootFolder = coreDataManager.readRecord(atPath: currentFolder)
        }
        
    }
    
    
    func getPathOfCurrentFolder() -> String {
        return currentFolder
    }
    
    private func updateInfo() {
        let folders: Array<FolderStruct> = coreDataManager.readRecords(type: coreDataManager.kFolderEntity) as! Array<FolderStruct>
        for var folder in folders {
            let folderInfo: FolderInfo = FileSystemManager.defaultManager.updateinfo(atPath: folder.path)
//            if
            var folderList: Array<String> = Array()
            var filesList: Array<String> = Array()
            for item in folderInfo.contents as! [String]  {
                if item.contains(".") {
                    filesList.append(item)
                } else {
                    folderList.append(item)
                }
            }
            folder.foldersList = folderList
            folder.filesList = filesList
            folder.contents = folderInfo.contents
            folder.size = folderInfo.size
            folder.folders = folder.foldersList.count
//            let count = folder.filesList.count
//            folder.count = count
            coreDataManager.update(folderInfo: folder)
        }
    }
    
    func changeCurrentFolder(atPath: String) {        
        FileSystemManager.defaultManager.change(currentPath: atPath)
        currentFolder = atPath
        debugPrint("Creator", currentFolder)
    }

    
    func createFolder(name: String) -> Bool! {
        var folder: FolderStruct! = FolderStruct()
        if FileSystemManager.defaultManager.createFolder(name: name) {
            
            folder.path = currentFolder + "/" + name
            folder.parent = currentFolder
            folder.name = name
            folder.date = Date()
            folder.type = coreDataManager.kFolderEntity
            
            coreDataManager.create(record: folder)
            var parent: FolderStruct = coreDataManager.readRecord(atPath: String(folder.parent.dropLast()))
            let folderInfo: FolderInfo = FileSystemManager.defaultManager.updateinfo(atPath: folder.parent)
            parent.contents = folderInfo.contents
            parent.folders = folderInfo.folders
            parent.size = folderInfo.size
            updateInfo()
            return true
        }
        return false
    }
    
    func deleteFolder(path: String) -> Bool {
        let folder = coreDataManager.readRecord(atPath: path)
        var contents: Array<String> = folder?.contents as! Array<String>
        if contents.count > 0 {
            while contents.count != 0 {
                let path = path + "/" + contents.first!
                let name: String = contents.first!
                if name.contains(".") {
                    if deleteFile(atPath: path) {
                        contents.removeFirst()
                         updateInfo()
                    }
                } else {
                    if deleteFolder(path: path) {
                        contents.removeFirst()
                         updateInfo()
                    }
                }
        }
        }
            if FileSystemManager.defaultManager.removeFolder(name: folder?.name, atParentPath: folder?.parent) {
                if  coreDataManager.delete(record: folder) {
                    updateInfo()
                    return true
                }
            }
        return false
        }
    
    func deleteFile(atPath: String) -> Bool {
        let file: FileStruct = coreDataManager.readRecord(atPath: atPath, type: coreDataManager.kFileEntity) as! FileStruct
        if FileSystemManager.defaultManager.removeFile(atPath: file.path) {
            if coreDataManager.delete(record: file) {
                updateInfo()
                return true
            }
        }
        return false
    }
    
    func createFile(name: String!, contents: Data!) -> Bool  {
        if FileSystemManager.defaultManager.createFile(name: name, data: contents) {
            var file: FileStruct = FileStruct()
            file.date = Date()
            file.name = name
            file.path = currentFolder + "/" + name
            file.size = FileSystemManager.defaultManager.getFileSize(atPath: file.path)
            file.type = coreDataManager.kFileEntity
            coreDataManager.create(record: file)
            updateInfo()
            return true
        }
        return false
    }
}


